export const navigation = [
  // {
  //   text: 'Home',
  //   path: '/home',
  //   icon: 'home'
  // },
  {
    text: 'Products',
    icon: 'folder',
    items: [
      {
        text: 'Manage',
        path: '/products',
        icon: 'cart'
      },
      {
        text: 'Charts',
        path: '/products/charts',
        icon: 'chart'
      }
    ]
  }
  // {
  //   text: 'Examples',
  //   icon: 'folder',
  //   items: [
  //     {
  //       text: 'Profile',
  //       path: '/profile'
  //     },
  //     {
  //       text: 'Display Data',
  //       path: '/display-data'
  //     }
  //   ]
  // }
];
