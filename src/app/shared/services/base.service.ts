import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import CustomStore, { CustomStoreOptions } from 'devextreme/data/custom_store';
import { environment } from 'src/environments/environment';
import { Observable, throwError, of } from 'rxjs';
import { catchError, take, map, timeout, retry, } from 'rxjs/operators';

export interface IMapMutationRequest{}
export interface IMapUpdateRequest extends IMapMutationRequest{}
export interface IMapInsertRequest extends IMapMutationRequest{}
export interface IMapDeleteRequest extends IMapMutationRequest{}

export interface IBaseServiceConfig {
    load?: {
        /**
         * url path
         */
        path?: string;
        /**
         * header options
         */
        httpOptions?: any;
        /**
         * test data prevent request
         */
        testData?: any;
        /**
         * map each object before load
         */
        // tslint:disable-next-line: ban-types
        mapItem?(item): void;
        /**
         * map request object before load
         */
        // tslint:disable-next-line: ban-types
        mapResult?(result): void;
        /**
         * onError request you can change emited error
         */
        onError?(error): void;
    };
    update?: {
        /**
         * url path
         */
        path?: string;
        /**
         * header options
         */
        httpOptions?: any;
        /**
         * map objet to send before send
         * @param variables - data object
         */
        mapUpdateRequest?(variables: any): IMapUpdateRequest;
        /**
         * onError request you can change emited error
         */
        onError?(error): void;
    };
    insert?: {
        /**
         * url path
         */
        path?: string;
        /**
         * header options
         */
        httpOptions?: any;
        /**
         * map objet to send before send
         * @param variables - data object
         */
        mapInsertRequest?(variables: any): IMapInsertRequest;
        /**
         * onError request you can change emited error
         */
        onError?(error): void;
    };
    delete?: {
        /**
         * url path
         */
        path?: string;
        /**
         * header options
         */
        httpOptions?: any;
        /**
         * map objet to send before send
         * @param variables - data object
         */
        mapDeleteRequest?(variables: any): IMapDeleteRequest;
        /**
         * onError request you can change emited error
         */
        onError?(error): void;
    };
}

@Injectable()
export class BaseService  {

    public activeRecord: any;

    constructor( public httpClient: HttpClient ) {}


    getCustomStore(path: string, storeConfig?: CustomStoreOptions, config?: IBaseServiceConfig){

        const defaultCustomStoreConfig = {
            key: 'id',
            load: (loadOptions: any) => {
                if (config.load?.testData){
                    return of(this.mapLoadResult(config.load.testData, config)).pipe(
                        catchError(error => {
                            if (config?.load?.onError){
                                error = config.load.onError(error);
                                throw new Error(error);
                            } else {
                                // throw new Error('Data Loading Error');
                                return this.errorHandl(error, config , 'load');
                            }
                        })
                    ).toPromise();
                }
                return this.Load(path, loadOptions, config).pipe(
                    catchError(error => {
                        if (config?.load?.onError){
                            error = config.load.onError(error);
                            throw new Error(error);
                        } else {
                            // throw new Error('Data Loading Error');
                            return this.errorHandl(error, config, 'load');
                        }
                    })
                ).toPromise();
            },
            insert: (values) => {
                return this.Create(path, values, config).pipe(
                    catchError(error => {
                        if (config?.insert?.onError){
                            error = config.insert.onError(error);
                            throw new Error(error);
                        } else {
                            // throw new Error('Inserting Error');
                            return this.errorHandl(error, config, 'insert');
                        }
                    })
                ).toPromise();
            },
            update: (key, values) => {
                return this.Update(path, key, values, config).pipe(
                    catchError(error => {
                        if (config?.update?.onError){
                            error = config.update.onError(error);
                            throw new Error(error);
                        } else {
                            // throw new Error('Updating Error');
                            return this.errorHandl(error, config, 'update');
                        }
                    })
                ).toPromise();
            },
            remove: (value) => {
                return this.Delete(path, value, config).pipe(
                    catchError(error => {
                        if (config?.delete?.onError){
                            error = config.delete.onError(error);
                            throw new Error(error);
                        } else {
                            // throw new Error('Deleting Error');
                            return this.errorHandl(error, config, 'remove');
                        }
                    })
                ).toPromise();
            },
            // errorHandler: (err) => {
            //     return this.errorHandl(err).toPromise();
            // }
        };
        const customStoreConfig = {...{}, ...defaultCustomStoreConfig, ...storeConfig};
        return new CustomStore(customStoreConfig);
    }

    Load(path: string, loadOptions: any, config?: IBaseServiceConfig): Observable<any>{
        const isNotEmpty = (value: any): boolean => {
            return value !== undefined && value !== null && value !== '';
        };
        let params: HttpParams = new HttpParams();
        [
                    'skip',
                    'take',
                    'requireTotalCount',
                    'requireGroupCount',
                    'sort',
                    'filter',
                    'totalSummary',
                    'group',
                    'groupSummary'
                ].forEach((i) => {
                    if (i in loadOptions && isNotEmpty(loadOptions[i])) {
                        params = params.set(i, JSON.stringify(loadOptions[i]));
                    }
                });
        let options = { params };
        if (config?.load?.httpOptions){
            options = {...params, ...config.load.httpOptions};
        }
        return this.httpClient.get(environment.api + path, options).pipe(
            take(1),
            timeout(environment.timeoutApiCalls.load),
            map(result => this.mapLoadResult(result, config))
        );
    }

    private mapLoadResult(result, config){
        const outResult = [];
        if (result && Array.isArray(result)){
            result.forEach(item => {
                if (config?.load.mapItem){
                    outResult.push(config.load.mapItem(item));
                } else {
                    outResult.push(item);
                }
            });
        }
        if (config.load.mapResult){
            return config.load.mapResult({
                data: outResult,
                totalCount: outResult.length,
                // summary: data.summary,
                // groupCount: data.groupCount
            });
        } else {
            return {
                data: outResult,
                totalCount: outResult.length,
                // summary: data.summary,
                // groupCount: data.groupCount
            };
        }
    }
    // POST
    Create(path: string, data: any, config?: IBaseServiceConfig): Observable<any> {
        if (config?.insert?.mapInsertRequest){
            data = config.insert.mapInsertRequest(data);
        }
        const url = environment.api  + path + (config?.insert?.path ?? '');
        return this.httpClient.post<any>(url, this.BindDataToServer(data), config?.insert?.httpOptions ?? undefined)
            .pipe(
            take(1),
            timeout(environment.timeoutApiCalls.insert)
        );
    }

    // PUT
    Update(path: string, id: any, data: any, config?: IBaseServiceConfig): Observable<any> {
        if (config?.update?.mapUpdateRequest){
            data = config.update.mapUpdateRequest(data);
        }
        const url = environment.api + path + (config?.update?.path ?? '') + id;
        // tslint:disable-next-line: max-line-length
        return this.httpClient.put<any>(url, this.BindDataToServer(data) , config?.update?.httpOptions ?? undefined)
        .pipe(
            take(1),
            timeout(environment.timeoutApiCalls.update)
        );

    }

    // DELETE
    Delete(path, id, config?: IBaseServiceConfig) {
        if (config?.delete?.mapDeleteRequest){
            id = config.delete.mapDeleteRequest(id);
        }
        const url = environment.api + path + (config?.delete?.path ?? '') + id;
        return this.httpClient.delete(url, config?.insert?.httpOptions ?? undefined)
        .pipe(
            take(1),
            timeout(environment.timeoutApiCalls.delete)
        );
    }

    protected BindDataToServer(data?: any): string {
        if (data) {
            return JSON.stringify(data);
        } else {
            return null;
        }
    }

    protected errorHandl(error, config?: IBaseServiceConfig, method?: string) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        } else {
            // Get server-side error
            switch (error.status) {
            case 401:
                errorMessage = `Error: Unauthorized`;
                break;
            case 403:
                errorMessage = `Error: Session Expired`;
                break;
            case 404:
                errorMessage = `Error: Reource not Found`;
                break;
            default:
                errorMessage = `Error Code: ${(error.status) ? error.status : 0 } - ${error.message ? error.message : method.toUpperCase() + ' Error'}`;
                break;
            }

        }
        return throwError(new Error(errorMessage));
    }
}
