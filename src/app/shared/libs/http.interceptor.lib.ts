
import { Observable, of} from 'rxjs';
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpInterceptor,
  HttpResponse,
} from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {


    constructor() {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
        request = request.clone({
            setHeaders: {
              'Content-Type': 'application/json'
            }
        });
        return next.handle(request).pipe(
            map((httpResponse: HttpResponse<any>) => {
                if (httpResponse instanceof HttpResponse) {
                    switch (httpResponse.status) {
                        case 200:
                            if ( typeof httpResponse.body === 'string'){
                                return httpResponse.clone({
                                    body: JSON.stringify({ message: httpResponse.body })
                                });
                            }
                            break;
                    }
                }
                return httpResponse;
            })
        );
    }

}
