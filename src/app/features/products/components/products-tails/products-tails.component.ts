import { Component, OnInit, NgModule,  ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DxTileViewModule, DxTileViewComponent } from 'devextreme-angular';
import { ProductsService } from '../../services/products.services';
import { FormsModule } from '@angular/forms';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'products-tails',
  templateUrl: './products-tails.component.html',
  styleUrls: ['./products-tails.component.scss']
})
export class ProductsTailsComponent implements OnInit {


  @ViewChild(DxTileViewComponent, { static: false }) tileView: DxTileViewComponent;

  constructor(public service: ProductsService){}

  ngOnInit(){ }


}



@NgModule({
  imports: [
    CommonModule,
    DxTileViewModule,
    FormsModule
  ],
  providers: [
    ProductsService
  ],
  declarations: [ ProductsTailsComponent ],
  exports: [ ProductsTailsComponent ]
})
export class ProductsTailsModule { }
