import { Component, OnInit, NgModule, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DxDataGridModule, DxDataGridComponent } from 'devextreme-angular';
import { ProductsService } from '../../services/products.services';
import { StoreChooserModule } from '../store-chooser/store-chooser.component';
import { FormsModule } from '@angular/forms';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'products-grid',
  templateUrl: './products-grid.component.html',
  styleUrls: ['./products-grid.component.scss']
})
export class ProductsGridComponent implements OnInit {


  @ViewChild('mainGrid', { static: false }) dataGrid: DxDataGridComponent;
  @ViewChild('detailGrid', { static: false }) detailGrid: DxDataGridComponent;

  isMasterDetailView = true;
  currentRowIndex = 0;

  btnClosePopupConfig = {
    text: 'Chiudi',
    onClick: (() => {
      if (this.dataGrid && this.dataGrid.instance){
        return this.dataGrid.instance.cancelEditData();
      }
    }).bind(this)
  };

  btnSavePopupConfig = {
    text: 'Salva',
    onClick: (() => {
      if (this.dataGrid && this.dataGrid.instance){
        const reviews = [];
        if (Array.isArray(this.service?.activeRecord?.reviews)){
          this.service.activeRecord.reviews.forEach(item => {
            reviews.push(item.commento);
          });
        }
        this.dataGrid.instance.cellValue(this.currentRowIndex, 'reviews', reviews);
        this.dataGrid.instance.cellValue(this.currentRowIndex, 'employee', this.service.employee);
        return this.dataGrid.instance.saveEditData();
      }
    }).bind(this)
  };

  btnCloseReviewPopupConfig = {
    text: 'Chiudi',
    onClick: (() => {
      if (this.dataGrid && this.detailGrid.instance){
        return this.detailGrid.instance.cancelEditData();
      }
    }).bind(this)
  };

  btnSaveReviewPopupConfig = {
    text: 'Salva',
    onClick: (() => {
      if (this.dataGrid && this.detailGrid.instance){
        return this.detailGrid.instance.saveEditData();
      }
    }).bind(this)
  };

  constructor(public service: ProductsService){ }

  ngOnInit(){ }


  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      widget: 'dxButton',
      options: {
          icon: 'refresh',
          hint: `Refresh`,
          onClick: ( e => {
            if (this.dataGrid.instance){
              this.dataGrid.instance.refresh();
              this.dataGrid.instance.repaint();
            }
          }).bind(this)
      }
    });
    e.toolbarOptions.items.unshift({
      location: 'after',
      widget: 'dxButton',
      options: {
          icon: 'revert',
          hint: `Reset grid settings`,
          onClick: (e => {
            if (this.dataGrid.instance){
              this.dataGrid.instance.clearFilter();
              this.dataGrid.instance.clearGrouping();
              this.dataGrid.instance.clearSelection();
              this.dataGrid.instance.clearSorting();
              this.dataGrid.instance.refresh();
              this.dataGrid.instance.repaint();
            }
          }).bind(this)
      }
    });
  }

  onEditingStart(e) {
    this.currentRowIndex = e.component.getRowIndexByKey(e.key);
    this.isMasterDetailView = false;
    this.service.activeRecord = e.data;
    if (!this.service.activeRecord?.reviews){
      this.service.activeRecord.reviews = [];
    }
  }

  onAddNewRow(e){
    this.currentRowIndex = 0;
    this.isMasterDetailView = false;
    this.service.activeRecord = {
      employee: this.service.store?.data?.employees[0] ?? 'N.D.',
      reviews: []
    };
  }

  onRowExpanding(e){
    this.isMasterDetailView = true;
    e.component.byKey(e.key).then(data=>{
      this.service.activeRecord = data;
      if (!this.service.activeRecord?.reviews){
        this.service.activeRecord.reviews = [];
      }
    });
  }

}



@NgModule({
  imports: [
    CommonModule,
    DxDataGridModule,
    StoreChooserModule,
    FormsModule
  ],
  providers: [
    ProductsService
  ],
  declarations: [ ProductsGridComponent ],
  exports: [ ProductsGridComponent ]
})
export class ProductsGridModule { }
