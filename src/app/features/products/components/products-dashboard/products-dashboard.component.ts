import { Component, OnInit, NgModule, Input, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DxDataGridModule, DxButtonModule, DxButtonComponent } from 'devextreme-angular';
import { ProductsService } from '../../services/products.services';
import { StoreChooserModule, StoreChooserComponent } from '../store-chooser/store-chooser.component';
import { ProductsGridModule, ProductsGridComponent } from '../products-grid/products-grid.component';
import { ProductsTailsModule, ProductsTailsComponent } from '../products-tails/products-tails.component';
import notify from 'devextreme/ui/notify';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'products-dashboard',
  templateUrl: './products-dashboard.component.html',
  styleUrls: ['./products-dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductsDashboardComponent implements OnInit, AfterViewInit {

  @ViewChild('btnGrid', { static: false }) btnGrid: DxButtonComponent;
  @ViewChild('btnTile', { static: false }) btnTile: DxButtonComponent;
  @ViewChild(ProductsGridComponent, { static: false }) productsGrid: ProductsGridComponent;
  @ViewChild(ProductsTailsComponent, { static: false }) productsTails: ProductsTailsComponent;
  @ViewChild(StoreChooserComponent, { static: false }) storeChooser: StoreChooserComponent;

  @Input()
  Layout: 'grid' | 'tails' | 'default';

  isIdentitySet = false;

  storeChooserMNotifyMessage = 'In order to load products data you must choose a store and user before';

  get gridStyle() {
    return { display: (this.isIdentitySet && this.Layout === 'grid') ? 'block' : 'none' };
  }

  get tileStyle() {
    return { display: (this.isIdentitySet && this.Layout === 'tails') ? 'block' : 'none'};
  }

  constructor(
    private ref: ChangeDetectorRef,
    public service: ProductsService
  ){}

  ngOnInit() {
    if (!this.Layout){
      this.Layout = 'default';
    }
    this.ref.detectChanges();
  }

  ngAfterViewInit(){
    if (!this.isIdentitySet){
      notify(this.storeChooserMNotifyMessage, 'warning', 5000);
    }
  }

  onValidation(e){
    this.isIdentitySet = e.isValid;
    if (e.isValid){
      if (this.Layout === 'default'){
        this.Layout = 'grid'; // setto layout predefinito di vista se scelto store e user
        this?.productsGrid?.dataGrid?.instance.option('dataSource', this.service.getProductsDataStore());
      }
    } else {
      this.Layout = 'default';
    }
    this.setButtonsEnabled();
    this.ref.detectChanges();
  }

  setButtonsEnabled(){
    if (this.Layout === 'grid'){
      this.btnGrid.instance.option('disabled', true);
      this.btnTile.instance.option('disabled', false);
    } else if (this.Layout === 'tails'){
      this.btnGrid.instance.option('disabled', false);
      this.btnTile.instance.option('disabled', true);
    } else {
      this.btnGrid.instance.option('disabled', false);
      this.btnTile.instance.option('disabled', false);
    }
  }

  onGridLayout(e){
    if (this.isIdentitySet){
      this.Layout = 'grid';
      this?.productsGrid?.dataGrid?.instance.option('dataSource', this.service.getProductsDataStore());
      this.setButtonsEnabled();
      this.ref.detectChanges();
    } else {
      this.storeChooser.Validate(true);
      notify(this.storeChooserMNotifyMessage, 'warning', 3000);
    }
  }

  onTailsLayout(e){
    if (this.isIdentitySet){
      this.Layout = 'tails';
      setTimeout(() => {
        this?.productsTails?.tileView?.instance.option('dataSource', this.service.getProductsDataStore());
      }, 50);
      this.setButtonsEnabled();
      this.ref.detectChanges();
    } else {
      this.storeChooser.Validate(true);
      notify(this.storeChooserMNotifyMessage, 'warning', 3000);
    }
  }

}

@NgModule({
  imports: [
    CommonModule,
    DxDataGridModule,
    StoreChooserModule,
    ProductsGridModule,
    ProductsTailsModule,
    DxButtonModule
  ],
  providers: [
    ProductsService
  ],
  declarations: [ ProductsDashboardComponent ],
  exports: [ ProductsDashboardComponent ]
})
export class ProductsDashboardModule { }
