
import { Component, OnInit, NgModule, Input, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsService } from '../../services/products.services';
import { DxPolarChartModule, DxSelectBoxModule, DxPolarChartComponent } from 'devextreme-angular';
import { StoreChooserModule, StoreChooserComponent } from '../store-chooser/store-chooser.component';
import notify from 'devextreme/ui/notify';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'products-chart-polar-area',
  templateUrl: './products-chart-polar-area.component.html',
  styleUrls: ['./products-chart-polar-area.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductsChartPolarAreaComponent implements OnInit, AfterViewInit {

  @ViewChild(StoreChooserComponent, { static: false }) storeChooser: StoreChooserComponent;
  @ViewChild(DxPolarChartComponent, { static: false }) polarChart: DxPolarChartComponent;

  types = ['scatter', 'line', 'area', 'bar', 'stackedbar'];

  isIdentitySet = false;

  get style() {
    return {display: this.isIdentitySet ? 'flex' : 'none'};
  }

  constructor(
    private ref: ChangeDetectorRef,
    public service: ProductsService
  ) {}

  ngOnInit(){
    this.service.getCategoriesDataSource();
    this.ref.detectChanges();
  }

  ngAfterViewInit(){
    this.isIdentitySet = this.storeChooser?.isValid;
    if (!this.isIdentitySet){
      notify('In order to load charts data you must choose a store and user before', 'warning', 5000);
    }
    this.ref.detectChanges();
  }

  onValidation(e){
    this.isIdentitySet = e.isValid;
    if (this.isIdentitySet){
      if (this.polarChart?.instance){
        this.polarChart.instance.option('dataSource', this.service.getCategoriesDataSource(e.storeId));
      }
    }
    this.ref.detectChanges();
  }
}

@NgModule({
  imports: [
    CommonModule,
    DxPolarChartModule,
    StoreChooserModule,
    DxSelectBoxModule
  ],
  providers: [
    ProductsService
  ],
  declarations: [ ProductsChartPolarAreaComponent ],
  exports: [ ProductsChartPolarAreaComponent ]
})
export class ProductsChartPolarAreaModule { }
