import { Component, OnInit, NgModule, Input, ViewChild, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  DxDropDownBoxModule,
  DxDropDownBoxComponent,
  DxDataGridModule,
  DxDataGridComponent,
  DxValidatorModule
} from 'devextreme-angular';
import { ProductsService } from '../../services/products.services';
import { DxValidationGroupComponent, DxValidationGroupModule } from 'devextreme-angular/ui/validation-group';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'store-chooser',
  templateUrl: './store-chooser.component.html',
  styleUrls: ['./store-chooser.component.scss']
})
export class StoreChooserComponent implements OnInit, AfterViewInit {

  @ViewChild('storeDD', { static: false }) dropdown: DxDropDownBoxComponent;
  @ViewChild('storeGRD', { static: false }) grid: DxDataGridComponent;

  @ViewChild('employeeDD', { static: false }) edropdown: DxDropDownBoxComponent;
  @ViewChild('employeeGRD', { static: false }) egrid: DxDataGridComponent;

  @ViewChild('targetGroup', {static: false}) validationGroup: DxValidationGroupComponent;

  gridDataSource: any;

  @Input()
  storeId: any[];

  @Input()
  employee: any[];

  // tslint:disable-next-line: no-output-on-prefix
  @Output()
  onStoreSelected: EventEmitter<any> = new EventEmitter();

  // tslint:disable-next-line: no-output-on-prefix
  @Output()
  onUserSelected: EventEmitter<any> = new EventEmitter();

  // tslint:disable-next-line: no-output-on-prefix
  @Output()
  onValidation: EventEmitter<any> = new EventEmitter();

  isValid = false;

  constructor(
    public service: ProductsService
  ){
    this.gridDataSource = this.service.getSoresDataSource();
  }

  ngOnInit(){
    this.storeId = this.service?.store?.id ? [this.service?.store?.id] : [];
    this.employee = this.service?.employee ? [this.service?.employee] : [];
  }

  ngAfterViewInit(){
    this.Validate();
  }

  onStoreSelectionChanged(event) {
    this.dropdown.instance.close();
    this.storeId = [];
    if (event.selectedRowsData[0]){
      this.storeId.push(event.selectedRowsData[0].id);
      this.service.store = event.selectedRowsData[0];
    } else {
      this.service.store = null;
    }
    // cambio origine dati dei dipendenti relativi allo store e azzero dipendente selezionato
    this.service.employees = [];
    if (this.service.store?.data?.employees){
      this.service.store?.data?.employees.forEach((employee) => {
        this.service.employees.push({
          key: employee,
          employee
        });
      });
    }
    // se il dipendente selezionato non esiste nella lista dei dipendenti dello store allora azzero la selezione
    if (this.service.employees.indexOf(this.employee[0]) === -1){
      this.service.employee = '';
      this.employee = [];
    }

  }

  _onStoreSelected(event) {
    this.onStoreSelected.emit({
      event,
      data: (this.storeId ? this.storeId[0]?.id : null)
    });
    this.Validate();
  }

  onStoreOpened(e){
    // se c'è stato un errore o comunque non ho alcun risultato provo a fare un reload
    // altrimenti tengo quello che ho già caricato al primo avvio
    if (this.grid?.instance?.totalCount() === -1){
      this.grid.instance.refresh();
    }
  }

  onEmployeeSelectionChanged(event) {
    this.edropdown.instance.close();
    this.employee = [];
    this.employee.push(event.selectedRowsData[0]?.key);
    this.service.employee = this.employee[0];
  }

  _onEmployeeSelected(event) {
    this.onUserSelected.emit({event, data: this.employee[0]});
    this.Validate();
  }

  onEmployeeOpened(e){
    // this.egrid.instance.refresh();
  }

  Validate(forceValidate?: boolean): boolean {
    this.isValid = false;
    if ( this.storeId?.length > 0 || this.employee?.length > 0 || forceValidate){
      const v = this.validationGroup?.instance.validate();
      if (this.storeId?.length > 0 && this.employee?.length > 0 && v?.isValid){
        this.isValid = true;
      }
    }
    this.onValidation.emit({
      isValid: this?.isValid,
      storeId: (this?.storeId ? this.storeId[0] : []),
      employee: (this?.employee ? this.employee[0] : '')
    });
    return this.isValid;
  }
}



@NgModule({
  imports: [
    CommonModule,
    DxDropDownBoxModule,
    DxDataGridModule,
    DxValidatorModule,
    DxValidationGroupModule
  ],
  providers: [
    ProductsService,
  ],
  declarations: [ StoreChooserComponent ],
  exports: [ StoreChooserComponent ]
})
export class StoreChooserModule { }
