import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ProductsService } from './services/products.services';
import { ProductsDashboardModule, ProductsDashboardComponent } from './components/products-dashboard/products-dashboard.component';
import { ProductsChartPolarAreaComponent } from './components/products-chart-polar-area/products-chart-polar-area.component';



const routes = [
  {
    path: 'products',
    component: ProductsDashboardComponent
  },
  {
    path: 'products/charts',
    component: ProductsChartPolarAreaComponent
  },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ProductsDashboardModule,
    RouterModule.forChild(routes)
  ],
  providers: [
      ProductsService
  ],
  exports: [
  ]
})
export class ProductsModule { }
