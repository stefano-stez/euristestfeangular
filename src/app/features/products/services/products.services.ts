import { Injectable } from '@angular/core';
import { BaseService } from 'src/app/shared/services/base.service';
import { HttpClient } from '@angular/common/http';
import CustomStore from 'devextreme/data/custom_store';

@Injectable({
  providedIn: 'root'
})
export class ProductsService extends BaseService {

    /**
     * origine dati stores
     */
    storesDataStore: CustomStore;

    productsCategoriesDataStore: CustomStore;
    /**
     * origine dati prodotti
     */
    categoriesDataStore: CustomStore;

    /**
     * origine dati prodotti
     */
    productsDataStore: CustomStore;
    /**
     * Record prodotto selezionato
     */
    activeRecord: any;
    /**
     * record negozio selezionato
     */
    store: any;

    /**
     * record utente selezionato
     */
    employee: any;

    /**
     * datasource of available emplyees
     */
    employees = [];

    constructor(httpClient: HttpClient){
        super(httpClient);
    }

    getProductsDataStore( storeId?: string){
        // ijpxNJLM732vm8AeajMR
        if (storeId) {
            this.store.id = storeId;
        }
        if (!this.store?.id){
            return null;
        }
        if (!this.productsDataStore){
            this.productsDataStore = this.getCustomStore(`stores/${this.store?.id}/products`, {
                                                                        loadMode: 'raw',
                                                                    }, {
                                                                        load: {
                                                                            mapItem: (item) => {
                                                                                if (item?.data?.reviews){
                                                                                    if (Array.isArray(item.data.reviews)){
                                                                                        // tslint:disable-next-line: max-line-length
                                                                                        item.data.reviews = item.data.reviews.map(commento => {
                                                                                            if (!(typeof commento === 'object') && !(commento?.commento)){
                                                                                                return  {commento};
                                                                                            }
                                                                                            return commento;
                                                                                        });
                                                                                    }
                                                                                }
                                                                                // tslint:disable-next-line: max-line-length
                                                                                return {...{}, ...{title: 'N.D.', description: 'N.D.', category: 'N.D.', price: 0.0, employee: 'N.D.', reviews: []}, ...item.data, ...{id: item.id}};
                                                                            },
                                                                            mapResult: (result) => {
                                                                                return result.data;
                                                                            },
                                                                            // testData: [
                                                                            //     {
                                                                            //         id: '01S9gCQgJ6zzsNQFBeOr',
                                                                            //         data: {
                                                                            //           category: 'wef',
                                                                            //           title: 'edfwe',
                                                                            //           description: 'wefwe',
                                                                            //           employee: 'efwef',
                                                                            //           price: 3.12
                                                                            //         }
                                                                            //       },
                                                                            //       {
                                                                            //         id: '6nu5s3Uo0Iisef5E0sWE',
                                                                            //         data: {
                                                                            //           category: 'torta',
                                                                            //           employee: 'Giovanni',
                                                                            //           price: 10.25,
                                                                            //           reviews: [
                                                                            //             'Non è vero che è buonissima, non mi piace proprio.',
                                                                            //             'È proprio buona!',
                                                                            //             'Ancora una, vi prego.'
                                                                            //           ],
                                                                            //           description: 'Torta buonissima della nonna alle mele.',
                                                                            //           title: 'Torta alle mele'
                                                                            //         }
                                                                            //       },
                                                                            //       {
                                                                            //         id: 'A4By8vkhm6kyC0B93Iv7',
                                                                            //         data: {
                                                                            //           category: 'wef',
                                                                            //           title: 'edfwe',
                                                                            //           description: 'wefwe',
                                                                            //           employee: 'efwef',
                                                                            //           price: 3.12
                                                                            //         }
                                                                            //       },
                                                                            //       {
                                                                            //         id: 'snVcUBOixyNW1PZC4h2i',
                                                                            //         data: {
                                                                            //           category: 'movie candies',
                                                                            //           title: 'Jelly bean',
                                                                            //           employee: 'Harry',
                                                                            //           price: 2.33
                                                                            //         }
                                                                            //       },
                                                                            //       {
                                                                            //         id: 'yHR8D9mCE4XdEFcH1hs1',
                                                                            //         data: {
                                                                            //           category: 'biscotti',
                                                                            //           employee: 'Giacomo',
                                                                            //           price: 6.3,
                                                                            //           reviews: [
                                                                            //             'Non mi piace lo zenzero.',
                                                                            //             'Sono troppo duri.',
                                                                            //             'Mi hanno fatto andare in ospedale.'
                                                                            //           ],
                                                                            //           description: 'Biscotti allo zenzero fatti in casa.',
                                                                            //           title: 'Biscotti allo zenzero'
                                                                            //         }
                                                                            //       },
                                                                            //       {
                                                                            //         id: 'yZdGefNEGqyJr5QRRpNF',
                                                                            //         data: {
                                                                            //           employee: 'Dieguito',
                                                                            //           price: 2.98,
                                                                            //           category: 'portoghese',
                                                                            //           title: 'Pasteis de nata'
                                                                            //         }
                                                                            //       }
                                                                            // ]
                                                                        },
                                                                        insert: {
                                                                            httpOptions: {
                                                                                responseType: 'text/html'
                                                                            },
                                                                        },
                                                                        update: {
                                                                            path: '/',
                                                                            mapUpdateRequest: (data) => {
                                                                                return data;
                                                                            }
                                                                        },
                                                                        delete: {
                                                                            path: '/'
                                                                        }
                                                                    });
        }
        return this.productsDataStore;
    }

    getSoresDataSource(){
        if (!this.storesDataStore){
            this.storesDataStore = this.getCustomStore(`stores`, {
                                        loadMode: 'raw',
                                        key: 'id',
                                    }, {
                                        load: {
                                            mapItem: (item) => {
                                                return {...{
                                                        store_name: item.data.name,
                                                        store_category: item.data.category
                                                    }, ...item
                                                };
                                            },
                                            mapResult: (result) => {
                                                return result.data;
                                            },
                                            // tslint:disable-next-line: max-line-length
                                            // testData: [{id: 'ijpxNJLM732vm8AeajMR', data: {name: 'Dolci di Piera', employees: ['Aldo', 'Giovanni', 'Giacomo'], category: 'pasticceria'}}]
                                        }
                                    });
        }
        return this.storesDataStore;
    }

    getCategoriesDataSource(storeId?: string){
        if (storeId) {
            this.store.id = storeId;
        }
        if (!this.store?.id){
            return null;
        }
        if (!this.categoriesDataStore){
            this.categoriesDataStore = this.getCustomStore(`stores/${this.store?.id}/stats/categories`, {
                loadMode: 'raw',
                key: 'category'
            }, {
                load: {
                    mapItem: (item) => {
                        return {...item, ...{arg: item?.category}};
                    },
                    mapResult: (result) => {
                        return result.data;
                    },
                    // tslint:disable-next-line: max-line-length
                    // testData: [{category: 'categoria nuovo', numberOfProducts: 1}, {category: 'torta',numberOfProducts: 1}, {category: 'cibo',numberOfProducts: 1}, {category: 'string',numberOfProducts: 1}, {category: 'movie candies',numberOfProducts: 1}, {category: 'categoria',numberOfProducts: 1}, {category: 'dsd',numberOfProducts: 1}, {category: 'biscotti',numberOfProducts: 1}, {category: 'portoghese',numberOfProducts: 1}]
                }
            });
        }
        return this.categoriesDataStore;
    }

    getProductCategoriesDataSource(storeId?: string){
        if (storeId) {
            this.store.id = storeId;
        }
        if (!this.store?.id){
            return null;
        }
        if (!this.productsCategoriesDataStore){
            this.productsCategoriesDataStore = this.getCustomStore(`stores/${this.store?.id}/stats/categories`, {
                loadMode: 'raw',
                key: 'category'
            }, {
                load: {
                    mapItem: (item) => {
                        return item.category;
                    },
                    mapResult: (result) => {
                        return result.data;
                    },
                    // tslint:disable-next-line: max-line-length
                    // testData: [{category: 'categoria nuovo', numberOfProducts: 1}, {category: 'torta',numberOfProducts: 1}, {category: 'cibo',numberOfProducts: 1}, {category: 'string',numberOfProducts: 1}, {category: 'movie candies',numberOfProducts: 1}, {category: 'categoria',numberOfProducts: 1}, {category: 'dsd',numberOfProducts: 1}, {category: 'biscotti',numberOfProducts: 1}, {category: 'portoghese',numberOfProducts: 1}]
                }
            });
        }
        return this.productsCategoriesDataStore;
    }

}
