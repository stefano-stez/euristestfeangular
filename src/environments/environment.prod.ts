export const environment = {
  production: true,
  api: 'http://us-central1-test-b7665.cloudfunctions.net/api/',
  timeoutApiCalls: {
    load: 5000,
    insert: 3000,
    update: 3000,
    delete: 3000
  }
};
